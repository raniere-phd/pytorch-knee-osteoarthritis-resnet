"""
Dataset

Includes all the functions that either preprocess or postprocess data.
"""
import os
import os.path

import pandas as pd
import torch.utils.data
from torchvision.io import read_image
import torchvision.transforms


class Dataset(torch.utils.data.Dataset):
    """Dataset"""

    def __init__(self, config):
        """
        :param config:
        """
        raise NotImplementedError

    def __len__(self):
        raise NotImplementedError

    def __getitem__(self, idx):
        raise NotImplementedError

    def plot_samples(self):
        """
        Plotting sample images
        """
        raise NotImplementedError

    def make_gif(self):
        """
        Make a gif from a multiple images
        """
        raise NotImplementedError

    def finalize(self):
        raise NotImplementedError


class DataLoader(torch.utils.data.DataLoader):
    """Data loader"""

    def __init__(self, batch_size, shuffle=True):
        """Create data loader instance."""
        super().__init__(
            Dataset(),
            batch_size,
            shuffle=shuffle
        )
