# PyTorch Knee Osteoarthritis ResNet

This repository is a Machine Learning model implemented in PyTorch
to detect fracture in the Knee Osteoarthritis data [1](#chen2018).

## Setting Environment with Conda

Run

```
$ conda env create --file environment.yml
```

## Data

The data is licensed licensed under a Creative Commons Attribution 4.0 International license.

### Mendeley

1. Visit https://data.mendeley.com/datasets/56rmx5bjcr/1.
1. Download the data and unzip it.

### Kaggle

1. Visit https://www.kaggle.com/shashwatwork/knee-osteoarthritis-dataset-with-severity.
1. Download the data and unzip it.


## Machine Learning Experiment

Run

```
$ python main.py
```

Metrics will be save in [Comet](https://www.comet.ml/).

## Files and Folders

- `agent.py`

  Has the knowledge to handle the data and model for train, validation and test.
- `data`

  Store the data. It should be replace with a symbolic link to the data.
- `dataset.py`

  Has the knowledge to load the data and create batches.
- `environment.yml`

  Conda environment.
- `main.py`

  Runnable script. It stores some hyperparameters.
- `model.py`

  Has the implementation of the model using PyTorch.
- `weights`

  Store the weights of trained model.

## Related Projects

- https://github.com/moemen95/PyTorch-Project-Template

## References

<a name="chen2018"></a>
[1] Chen, Pingjun (2018), “Knee Osteoarthritis Severity Grading Dataset”, Mendeley Data, V1, doi: 10.17632/56rmx5bjcr.1